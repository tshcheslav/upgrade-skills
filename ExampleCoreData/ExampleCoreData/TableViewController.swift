//
//  TableViewController.swift
//  ExampleCoreData
//
//  Created by 1 on 12.01.2022.
//

import UIKit
import CoreData

class TableViewController: UITableViewController {
    var tasks: [Tasks] = []
    
    @IBAction func addTask(_ sender: UIBarButtonItem) {
        let alertController = UIAlertController(title: "NewTask", message: "AddIt?", preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "add", style: .default) { action in
            let tf = alertController.textFields?.first
            let tf2 = alertController.textFields?.last
            if let newTask = tf?.text, let newTask2 = tf2?.text {
//                self.tasks.insert(newTask, at: 0)
                self.saveTask(withTitle: newTask, name: newTask2)
                self.tableView.reloadData()
            }
        }
        
        alertController.addTextField { _ in}
        alertController.addTextField { _ in}
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    // сохранение
    
    func saveTask(withTitle title: String, name: String) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        guard let entity = NSEntityDescription.entity(forEntityName: "Tasks", in: context) else {return}
        
        let taskObject = Tasks(entity: entity, insertInto: context)
        taskObject.title = title
        taskObject.name = name
        do {
            try context.save()
            tasks.append(taskObject)
        } catch  let error as NSError {
            print(error.localizedDescription)
        }
                
    }
    
    // запрос
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let fetchRequest: NSFetchRequest<Tasks> = Tasks.fetchRequest()
        
        do {
            tasks = try context.fetch(fetchRequest)
            
        }catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    // MARK: - Table view data source

  

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasks.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? newTableViewCell
        
        let task = tasks[indexPath.row]
        cell?.lab2.text = task.title
        cell?.lab.text = task.name

        return cell!
    }
    

 
}
