//
//  ViewController.swift
//  WebSocketExample
//
//  Created by 1 on 04.01.2022.
//

import UIKit

class ViewController: UIViewController , URLSessionWebSocketDelegate {

    private var webSocket: URLSessionWebSocketTask?
    
    private let closeButton: UIButton = {
       let button = UIButton()
        button.backgroundColor = .systemCyan
        button.setTitle("Close", for: .normal)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
    
        
        view.backgroundColor = .systemBlue
        
        let session = URLSession(
            configuration: .default,
            delegate: self,
            delegateQueue: OperationQueue())
        
        let url = URL(string: "wss://demo.piesocket.com/v3/channel_1?api_key=oCdCMcMPQpbvNjUIzqtvF1d2X2okWpDQj4AwARJuAgtjhzKxVEjQU6IdCjwm&notify_self")
        
        webSocket = session.webSocketTask(with: url!)
        webSocket?.resume()
        
        closeButton.frame = CGRect(x: 500, y: 100, width: 200, height: 50)
        view.addSubview(closeButton)
        closeButton.addTarget(self, action: #selector(close), for: .touchUpInside)
        closeButton.center = view.center
    }


    func ping() {
        webSocket?.sendPing(pongReceiveHandler: { error in
            if let error = error {
                print("Ping error: \(error)")
            }
        })
    }
    
    @objc func close() {
        webSocket?.cancel(with: .goingAway, reason: "Demo ended".data(using: .utf8))
    }
    
    func send() {
        DispatchQueue.global().asyncAfter(deadline: .now()+1) {
            self.send()
            self.webSocket?.send(.string("Send new message: \(Int.random(in: 0...1000))"), completionHandler: { error in
                if let error = error {
                    print("Send error: \(error)")
                }
            })
        }
    }
    
    func recive() {
        webSocket?.receive(completionHandler: {[weak self] result in
            switch result {
            case .success(let message):
                switch message {
                case .data(let data):
                    print("Go data: \(data)")
                case .string(let message):
                    print("Got String: \(message)")
                @unknown default:
                    break
                }
            case .failure(let error):
                print("Recive error \(error)")
            }
            self?.recive()
        })
    }
    
    
    func urlSession(_ session: URLSession, webSocketTask: URLSessionWebSocketTask, didOpenWithProtocol protocol: String?) {
        print("Did connect to socket")
        ping()
        recive()
        send()
    
        
    }
    
    func urlSession(_ session: URLSession, webSocketTask: URLSessionWebSocketTask, didCloseWith closeCode: URLSessionWebSocketTask.CloseCode, reason: Data?) {
        print("Did close connect with reason")
    }
}

