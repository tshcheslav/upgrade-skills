//
//  ViewController.swift
//  MVVMexample
//
//  Created by 1 on 09.01.2022.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var loginField: UITextField!
   
    @IBOutlet weak var label: UILabel!
    @IBAction func loginButton(_ sender: UIButton) {
        viewModel.userButtonPressed(login: (loginField.text ?? "") , password: passwordField.text ?? "")
    }
    var viewModel = ViewModel()
    
    func initialState() {
        label.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bindViewModel()
        initialState()
    }

    
    
    func bindViewModel() {
        viewModel.statusText.bind ({ statusText in
            DispatchQueue.main.async {
                self.label.text = statusText
                
            }
        })
        viewModel.statusColor.bind { statusColor in
            DispatchQueue.main.async {
                self.label.textColor = statusColor
            }
        }
    }
}

