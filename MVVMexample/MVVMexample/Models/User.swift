//
//  User.swift
//  MVVMexample
//
//  Created by 1 on 09.01.2022.
//

import Foundation

struct User {
    let login : String?
    let password : String?
}

extension User {
    static var logins = [
        User(login: "lexone", password: "12345")
    ]
}
