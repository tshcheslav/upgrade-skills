//
//  ViewController.swift
//  GoogleMapsExample
//
//  Created by 1 on 07.01.2022.
//

import CoreLocation
import UIKit
import GoogleMaps

class ViewController: UIViewController, CLLocationManagerDelegate  {
    
    
    let manager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()
        
        GMSServices.provideAPIKey("AIzaSyDdluW7A6P8TyqdsUAbmibD6l9jqkxxRz8")
        
       
        print("License:\n\n\(GMSServices.openSourceLicenseInfo())")
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {return}
        
        let coordinate = location.coordinate
        
        
        let camera = GMSCameraPosition.camera(withLatitude: coordinate.latitude, longitude: coordinate.longitude, zoom: 1.0)
        let mapView = GMSMapView.map(withFrame: view.frame, camera: camera)
       view.addSubview(mapView)
        
                let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: coordinate.latitude, longitude: coordinate.longitude)
        marker.title = "Sydney"
        marker.snippet = "Australia"
        marker.map = mapView

    }
 
}

