//
//  ModelUser.swift
//  MapLocation
//
//  Created by 1 on 08.01.2022.
//

import Foundation
import UIKit
import MapKit

class ModelUser{
    var users = [[User]]()
    init() {
        setup()
    }
    
    func setup() {
        let manOne = User(name: "Kolya", city: "Moscow", image: UIImage(named: "pencil")! , gender: .male, coordinate: CLLocationCoordinate2D(latitude: 37.765834, longitude: -122.403417))
        let manTwo = User(name: "Tolya", city: "Lev", image: UIImage(named: "pencil")! , gender: .male, coordinate: CLLocationCoordinate2D(latitude: 37.765839, longitude: -122.403417))
        
        let manArray = [manOne, manTwo]
        
        let womanOne = User(name: "Katya", city: "Piter", image: UIImage(named: "pencil")!, gender: .male, coordinate: CLLocationCoordinate2D(latitude: 37.715834, longitude: -122.406417))
        let womanTwo = User(name: "Olya", city: "Kiev", image: UIImage(named: "pencil")! , gender: .male, coordinate: CLLocationCoordinate2D(latitude: 37.762834, longitude: -122.4023417))
        
        let womanArray = [womanOne, womanTwo]
    
        users.append(manArray)
        users.append(womanArray)
    }
}
