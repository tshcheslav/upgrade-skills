//
//  MainViewController.swift
//  MyPlaceFinder
//
//  Created by 1 on 12.01.2022.
//

import UIKit

class MainViewController: UIViewController {

    @IBAction func twoPerson(_ sender: UIButton) {
       performSegue(withIdentifier: "twoPerson", sender: nil)
    }
    
    @IBAction func OnePerson(_ sender: UIButton) {
        performSegue(withIdentifier: "onePerson", sender: nil)
    }
    
    @IBAction func historyTo(_ sender: UIButton) {
        performSegue(withIdentifier: "historyRecords", sender: nil)
    }
    
    @IBAction func findLocation(_ sender: UIButton) {
        performSegue(withIdentifier: "searcher", sender: nil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
    }
    



}
