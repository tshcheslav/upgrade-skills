//
//  MainProjectViewController.swift
//  MyPlaceFinder
//
//  Created by 1 on 14.01.2022.
//

import UIKit
import CoreLocation

class MainProjectViewController: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var latLabel: UILabel!
    @IBOutlet weak var lonLabel: UILabel!
    
    var pointOneX = 0.0
    var pointOneY = 0.0
 
    @IBAction func go(_ sender: UIButton) {
        performSegue(withIdentifier: "One", sender: nil)
    }
    
 private let manager = CLLocationManager()
    override func viewDidLoad() {
        super.viewDidLoad()
        latLabel.text = "0.0"
        lonLabel.text = "0.0"
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        manager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        manager.delegate = self
        manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        manager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
//            manager.stopUpdatingLocation()
            
            self.latLabel.text = String(location.coordinate.latitude)
            self.lonLabel.text = String(location.coordinate.longitude)
            self.pointOneX = location.coordinate.latitude
            self.pointOneY = location.coordinate.longitude
            print(locations)
            print(pointOneX)
            print(pointOneY)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "One" {
            if let vc = segue.destination as? MainProjectTwoViewController {
            vc.latOne = pointOneX
            vc.lonOne = pointOneY
            }
        }
    }
    


}
