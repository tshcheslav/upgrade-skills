//
//  MainProjectTwoViewController.swift
//  MyPlaceFinder
//
//  Created by 1 on 14.01.2022.
//

import UIKit
import Foundation
import CoreLocation

class MainProjectTwoViewController: UIViewController, CLLocationManagerDelegate {
    
    var latOne = 0.0
    var lonOne = 0.0
    var latTwo = 0.0
    var lonTwo = 0.0
    var distance = 0.0
    
  private let manager = CLLocationManager()
    
    @IBOutlet weak var latLabel: UILabel!
    @IBOutlet weak var lonLabel: UILabel!
    
    @IBAction func go(_ sender: UIButton) {
        performSegue(withIdentifier: "Two", sender: nil)
        manager.stopUpdatingLocation()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        manager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        manager.delegate = self
        manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        manager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
//            manager.stopUpdatingLocation()
            
            self.latLabel.text = String(location.coordinate.latitude)
            self.lonLabel.text = String(location.coordinate.longitude)
            self.latTwo = location.coordinate.latitude
            self.lonTwo = location.coordinate.longitude
            print(locations)
            print(latTwo)
            print(lonTwo)
            let coordinateT = CLLocation(latitude: latOne, longitude: lonOne)
            let coordinateTh = CLLocation(latitude: latTwo, longitude: lonTwo)
            self.distance = coordinateT.distance(from: coordinateTh) / 1000
            print(location)
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Two" {
            if let vcT = segue.destination as? MainProjectThreeViewController{
            vcT.distance = distance
            }
        }
    }
}
