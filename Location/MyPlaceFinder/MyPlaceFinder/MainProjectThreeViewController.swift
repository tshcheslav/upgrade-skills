//
//  MainProjectThreeViewController.swift
//  MyPlaceFinder
//
//  Created by 1 on 14.01.2022.
//

import UIKit
import CoreLocation
import CoreData

class MainProjectThreeViewController: UIViewController {
    private let manager = CLLocationManager()
    var distance = 0.0
    var perimetr: [Squares] = []
    @IBOutlet weak var spacing: UILabel!
    
    @IBOutlet weak var nameTF: UITextField!
    
    func saveAnswear (title: String, num: String) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        guard let entity = NSEntityDescription.entity(forEntityName: "Squares", in: context) else {return}
        
        let answearObject = Squares(entity: entity, insertInto: context)
        answearObject.name = title
        answearObject.square = num
        do {
            try context.save()
            perimetr.append(answearObject)
        } catch  let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    
    
    @IBAction func goToTheRootVC(_ sender: UIButton) {
        if nameTF.text!.count > 0 {
            let title = nameTF.text
            let num = spacing.text
            saveAnswear(title: title!, num: num!) } else {
            let title = "No Name"
            let num = spacing.text
            saveAnswear(title: title, num: num!)
        }
       performSegue(withIdentifier: "goToStory", sender: nil)
      
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let round = String(distance)
        let round2 = round.prefix(8)
        self.spacing.text = String(round2) + " kм"
        manager.stopUpdatingLocation()
    }
    


    

}
